import 'dart:ui';

import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    var resultText = 'You did it!';
    if (resultScore <= 8) {
      resultText = 'You are awesome and cool!';
    } else if (resultScore <= 12) {
      resultText = 'Pretty good job there!';
    } else if (resultScore <= 20) {
      resultText = 'You are ... super!';
    } else {
      resultText = 'You are badass!!';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          TextButton(
            child: Text(
              'Restart Quiz!',
              style: TextStyle(fontSize: 24),
            ),
            style: TextButton.styleFrom(
              primary: Colors.lightGreen,
            ),
            onPressed: resetHandler,
          )
        ],
      ),
    );
  }
}
